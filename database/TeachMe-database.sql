-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2015 at 02:57 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `teachme`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `ID` int(11) NOT NULL,
  `type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `ID` int(11) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `body` text NOT NULL,
  `upvotes` float NOT NULL,
  `downvotes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `ID` int(11) NOT NULL,
  `postID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `status` varchar(30) NOT NULL,
  `comment` text NOT NULL,
  `commentType` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`ID`, `postID`, `userID`, `status`, `comment`, `commentType`, `timestamp`) VALUES
(1, 1, 1, '1', 'Some Comment', 1, '2015-11-05 12:08:30'),
(2, 1, 2, '1', 'Sounds like a plan', 1, '2015-11-05 12:17:00'),
(3, 1, 1, '1', 'Yeah I like that', 1, '2015-11-05 12:24:44'),
(4, 2, 2, '1', 'I am commenting', 1, '2015-11-06 01:21:05'),
(5, 2, 1, '1', 'test', 1, '2015-11-06 01:27:19');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `ID` int(11) NOT NULL,
  `accNo` int(11) NOT NULL,
  `value` double NOT NULL,
  `type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gig`
--

CREATE TABLE IF NOT EXISTS `gig` (
  `ID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `vote` int(11) NOT NULL DEFAULT '0',
  `category` varchar(30) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `location` varchar(100) NOT NULL,
  `type` varchar(30) DEFAULT NULL,
  `tags` text NOT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0 - Open 1 - In Progress 2 - Getting Feed back 3 - Closed 4 - Cancel',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gig`
--

INSERT INTO `gig` (`ID`, `userID`, `title`, `description`, `vote`, `category`, `longitude`, `latitude`, `location`, `type`, `tags`, `status`, `timestamp`) VALUES
(1, 1, 'I need to LEarn PHP', 'Learning something from scratch is almost always an arduous affair -- you simply have no idea as to where to start, or not to start, to kick things off. I loathed learning about the idiosyncrasies of C++''s syntax when all I wanted to learn were some darn programming concepts. As I''m sure you can agree, this is a less than ideal situation.\n\nThis is where the Nettuts+ "The Best Way to Learn" series comes into the picture. This series is intended to be your blueprint, your road map, your plan of action for learning your topic of choice! You don''t have to worry about finding the best resources, sorting out the bad ones, and figuring out what to learn next. It''s all here. Just follow it, step by step.\n\nToday, we''re going to figure out the best way to learn PHP.', 0, '2', 79.987832, 6.8992509841918945, 'Colombo LK', NULL, '1 2 3', 0, '2015-11-05 11:38:13'),
(2, 2, 'Help me learn JavaScript', 'Can someone help me to learn JavaScript', 0, '2', 79.987831, 6.8992509841918945, 'Colombo LK', NULL, '1 2 3', 0, '2015-11-05 12:59:56'),
(3, 3, 'I need some help in Maths', 'I need some help in Maths', 0, '2', 80.220977, 6.053518, 'Galle LK', NULL, '1 2 3', 0, '2015-11-05 13:16:27'),
(4, 3, 'I need some help in Cookery', 'I need some help in Cookery', 0, '1', 80.214495, 6.061342, 'Galle LK', NULL, '1 2 3', 0, '2015-11-05 13:22:02'),
(5, 4, 'I need some help in Cookery', 'I need some help in Cookery', 0, '1', 80.214495, 6.061342, 'Galle LK', NULL, '1 2 3', 0, '2015-11-05 13:22:21'),
(6, 5, 'I need some help in Maths', 'I need some help in Maths', 0, '2', 80.22097729999996, 6.0535185, 'Galle LK', NULL, '1 2 3', 0, '2015-11-05 13:51:40'),
(7, 4, 'I need some help in', 'I need some help in', 0, '1', 80.214495, 6.061342, 'Galle LK', NULL, '1 2 3', 0, '2015-11-05 16:06:03');

-- --------------------------------------------------------

--
-- Table structure for table `group_gig`
--

CREATE TABLE IF NOT EXISTS `group_gig` (
  `ID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `gigID` int(11) NOT NULL COMMENT 'Real Group ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `ID` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`ID`, `name`) VALUES
(1, 'Basic Numeracy '),
(2, 'HTML '),
(3, 'PHP'),
(4, 'JavaScript'),
(5, 'Java'),
(6, '3DsMax');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `ID` int(11) NOT NULL,
  `amount` double NOT NULL,
  `fromUser` int(11) NOT NULL,
  `toUser` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL,
  `rating` double NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `reputation` int(11) DEFAULT '0',
  `tCoins` decimal(10,2) NOT NULL DEFAULT '0.00',
  `DoB` date NOT NULL,
  `email` varchar(40) NOT NULL,
  `longitude` float NOT NULL,
  `latitude` float NOT NULL,
  `skills` text,
  `role` varchar(30) NOT NULL DEFAULT 'U',
  `status` int(1) NOT NULL DEFAULT '1',
  `password` varchar(50) NOT NULL,
  `tSpending` decimal(10,2) DEFAULT '0.00'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `rating`, `name`, `reputation`, `tCoins`, `DoB`, `email`, `longitude`, `latitude`, `skills`, `role`, `status`, `password`, `tSpending`) VALUES
(1, 0, 'Udara Karunarathna', 0, '50.00', '1993-11-25', 'udarapathmin@gmail.com', 55, 45, 'test', 'U', 1, '5cb0a91487cea0e6f998f56c61e85aff', '0.00'),
(2, 0, 'Sachethana', 0, '50.00', '1993-10-25', 'sachethana@gmail.com', 45, 75, NULL, 'U', 1, 'a4bad1a6ebe9f9eecc24cc4c88aeaf68', '0.00'),
(3, 0, 'James Kingston', 0, '50.00', '1993-10-25', 'james@gmail', 45, 75, NULL, 'U', 1, 'b4cc344d25a2efe540adbf2678e2304c', '0.00'),
(4, 0, 'Yasas', 0, '50.00', '1993-10-25', 'yasas@gmail', 45, 75, NULL, 'U', 1, 'd00459b873bbd3e3083f34cb516c5d2e', '0.00'),
(5, 0, 'Sasika', 0, '50.00', '1993-10-25', 'sasika@gmail', 45, 75, NULL, 'U', 1, '0bc73c18d88d9c893066cafeed2da344', '0.00'),
(6, 0, 'Sachethana', 0, '50.00', '1993-10-25', 'kavisha@gmail.com', 45, 75, NULL, 'U', 1, 'b9afde99fab2932ef86e67e52536fba0', '0.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`ID`), ADD KEY `articleID` (`postID`,`userID`), ADD KEY `userID` (`userID`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`ID`), ADD KEY `accNo` (`accNo`);

--
-- Indexes for table `gig`
--
ALTER TABLE `gig`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `group_gig`
--
ALTER TABLE `group_gig`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`ID`), ADD KEY `accno` (`fromUser`,`toUser`), ADD KEY `userID` (`toUser`), ADD KEY `userID_2` (`toUser`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gig`
--
ALTER TABLE `gig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `group_gig`
--
ALTER TABLE `group_gig`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
