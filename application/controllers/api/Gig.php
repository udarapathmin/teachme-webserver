<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * TeachMe Web Services
 **/
class Gig extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Gig_model');

    }

    // Add new Gig
    public function addGig_post()
    {
        //title,description,category,longitude,latitude,location,tags
        $userID =  $this->post('userID');
        $title = $this->post('title');
        $description = $this->post('description');
        $category = $this->post('category');
        $longitude = $this->post('longitude');
        $latitude = $this->post('latitude');
        $location = $this->post('location');
        $tags = $this->post('tags');
        $gig = array('userID' => $userID, 'title' => $title, 'description' => $description, 'category' => $category, 'longitude' => $longitude, 'latitude' => $latitude, 'location' => $location, 'tags' => $tags);
        $gig_Status = $this->Gig_model->addgig($gig);

        if($gig_Status != false){
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Status"=>true,"Timestamp" => $timestamp);
            $this->response($return_array);
        }else{
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Status"=>false,"Timestamp" => $timestamp);
            $this->response($return_array);
        }
    }

    // Get Gig
    public function getGig_get($id)
    {
        $gig_Data = $this->Gig_model->getgig($id);
        $timestamp = date('Y-m-d H:i:s');
        $return_array = array("Gig"=>$gig_Data, "Status"=>true,"Timestamp" => $timestamp);
        $this->response($return_array);
    }

    // Get Gig Comments
    public function getGigComments_get($id)
    {
        $gig_Data = $this->Gig_model->getgigcomments($id);
        $timestamp = date('Y-m-d H:i:s');
        $return_array = array("Comments"=>$gig_Data, "Status"=>true,"Timestamp" => $timestamp);
        $this->response($return_array);
    }

    //Addd Comments
    public function addComment_post()
    {
        //postID,userID,status,comment,commentType,timestamp        
        $postID = $this->post('postID');
        $userID = $this->post('userID');
        $status = 1;
        $comment = $this->post('comment');
        $commentType = $this->post('commentType');

        $comment = array('postID' => $postID, 'userID' => $userID, 'status' => $status, 'comment' => $comment, 'commentType' => $commentType);
        $comment_Status = $this->Gig_model->addcomment($comment);

        if($comment_Status != false){
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Status"=>true,"Timestamp" => $timestamp);
            $this->response($return_array);
        }else{
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Status"=>false,"Timestamp" => $timestamp);
            $this->response($return_array);
        }
    }

    // Get Gig Comments
    public function getGigFeed_get($latitude, $longitude)
    {
        $gig_Data = $this->Gig_model->getgigs_around_me($latitude, $longitude);
        $timestamp = date('Y-m-d H:i:s');
        $return_array = array("Gigs"=>$gig_Data, "Status"=>true,"Timestamp" => $timestamp);
        $this->response($return_array);
    }

    // Add user to Group
    public function addUserToGroup_post()
    {
        //postID,userID,status,comment,commentType,timestamp        
        $gigID = $this->post('gigID');
        $userID = $this->post('userID');

        $object = array('gigID' => $gigID, 'userID' => $userID);
        $status = $this->Gig_model->add_user_to_group($object);

        if($status != false){
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Status"=>true,"Timestamp" => $timestamp);
            $this->response($return_array);
        }else{
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Status"=>false,"Timestamp" => $timestamp);
            $this->response($return_array);
        }
    }

    public function getSkills_get()
    {
        $skill_Data = $this->Gig_model->getskills();
        $timestamp = date('Y-m-d H:i:s');
        $return_array = array("Skills"=>$skill_Data, "Status"=>true,"Timestamp" => $timestamp);
        $this->response($return_array);
    }

}
