<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * TeachMe Web Services
 **/
class Users extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('User_model');

    }

    public function login_post()
    {
        $email = $this->post('email');
        //Password
        $password = $this->post('password');
        $password = md5($password);
        $user = $this->User_model->login($email, $password);

        if($user != false){
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("user"=>$user,"Status"=>true,"Timestamp" => $timestamp);
            $this->response($return_array);
        }else{
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Status"=>false,"Timestamp" => $timestamp);
            $this->response($return_array);
        }
    }

    public function register_post()
    {
        // name,email,password,tCoins,DoB,longitude,latitude
        $name = $this->post('name');
        $email = $this->post('email');
        $password = $this->post('password');
        $password = md5($password);
        $DoB = $this->post('DoB');
        $latitude = $this->post('latitude');
        $longitude = $this->post('longitude');
        $tCoins = $this->post('tCoins');

        $user_arr = array('name' => $name, 'email' => $email, 'password' => $password, 'tCoins' => $tCoins, 'DoB' => $DoB, 'longitude' => $longitude, 'latitude' => $latitude );

        $register = $this->User_model->register($user_arr);

        if($register != false){
            $timestamp = date('Y-m-d H:i:s');
            $user_data = $this->User_model->getuser($email);
            $return_array = array("User"=>$user_data, "Status"=>true,"Timestamp" => $timestamp);
            $this->response($return_array);
        }else{
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Message"=>"Email is already Registered","Status"=>false,"Timestamp" => $timestamp);
            $this->response($return_array);
        }
    }

    // Update User Location
    public function updateLocation_post()
    {
        // userID,longitude,latitude
        $userID = $this->post('userID');
        $longitude = $this->post('longitude');
        $latitude = $this->post('latitude');

        $Location = array('longitude' => $longitude, 'latitude' => $latitude);

        $status = $this->User_model->updatelocation($Location, $userID);

        if($status != false){
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Status"=>true,"Timestamp" => $timestamp);
            $this->response($return_array);
        }else{
            $timestamp = date('Y-m-d H:i:s');
            $return_array = array("Status"=>false,"Timestamp" => $timestamp);
            $this->response($return_array);
        }
    }

    // Get User Profile
    public function getUser_get($id)
    {
        $userdata = $this->User_model->getuserbyid($id);
        $timestamp = date('Y-m-d H:i:s');
        $return_array = array("User"=>$userdata, "Status"=>true,"Timestamp" => $timestamp);
        $this->response($return_array);
    }

}
