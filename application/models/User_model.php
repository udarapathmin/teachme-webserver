<?php
// Coded by UDara Karunarathna
class User_model extends CI_Model {

    public function __construct() {
            $this->load->database();
    }

    // User Login
    public function login($email, $password) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function register($user) {
        $email = $user['email'];
        $sql = "SELECT * FROM user WHERE email = '$email'";
        $query = $this->db->query($sql);
        if ($query->num_rows() == 0){
            if ($this->db->insert('user', $user)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else{
            return FALSE;
        }
    }

    // User Login
    public function getuser($email) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {
            return FALSE;
        }
    }

    function updatelocation($data, $userid){

        $this->db->where('ID', $userid);
        if ($this->db->update('user', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // User Login
    public function getuserbyid($id) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('ID', $id);
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {
            return FALSE;
        }
    }

}
    