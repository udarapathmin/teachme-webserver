<?php

class Gig_model extends CI_Model {

    public function __construct() {
            $this->load->database();
    }

    public function addgig($gig) {
        if ($this->db->insert('gig', $gig)){
            return TRUE;
        } else{
            return FALSE;
        }
    }

    public function getgig($id) {
        $this->db->select('gig.*,user.name');
        $this->db->from('gig');
        $this->db->where('gig.ID', $id);
        $this->db->join('user', 'user.ID = gig.userID');
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {

            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function getgigcomments($id) {
        $this->db->select('comments.*,user.name');
        $this->db->from('comments');
        $this->db->where('postID', $id);
        $this->db->where('commentType', "1");
        $this->db->join('user', 'user.ID = comments.userID');
        $this->db->order_by("timestamp", "asc"); 

        $query = $this->db->get();

        return $query->result();
    }

    public function addcomment($comment) {
        if ($this->db->insert('comments', $comment)){
            return TRUE;
        } else{
            return FALSE;
        }
    }



    public function getgigs_around_me($lat, $long) {
        // Radius Search Query found on http://stackoverflow.com/questions/12949476/get-latitude-and-longitude-of-all-location-that-are-within-10km-radius-of-my-cur/19388601#19388601
        //  Credits to user Santosh Yadav
        $distanceInMile = 12.4274;
        $centerpoints['lat'] = $lat;
        $centerpoints['lng'] = $long;
        $query = $this->db->query("select * from gig where latitude!='' and longitude!='' and ( 3959 * acos( cos( radians(" . $centerpoints['lat'] . ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" . $centerpoints['lng'] . ") ) + sin( radians(" . $centerpoints['lat'] . ") ) * sin( radians( latitude ) ) ) ) < " . $distanceInMile);

        return $query->result();
    }

    public function getskills() {
        $this->db->select('*');
        $this->db->from('skills');
        $this->db->order_by("name", "asc"); 

        $query = $this->db->get();

        return $query->result();
    }
}
    